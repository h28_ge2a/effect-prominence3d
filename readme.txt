cocos2d-xの授業で紹介したエフェクトを作るソフト「Prominence」の３Ｄ版「Prominence3D」がバージョンアップして
作ったエフェクトをDirectXで表示できるようになってました。

ためしに戦車ゲームで使ってみたサンプルです。
前進すると砂煙が出て、スペースキーでビームが撃てます。
Tank.hとTank.cppのみいじってます。


Prominence3Dはこちらからダウンロード
http://game-hikidashi.com/prominence3d/
エフェクトを作成するのは無料でできますが、
DirectXで使うためには有料のシリアルコードが必要です。
数人分用意してあるので、欲しい人は言ってください。


DirectXで使う方法はこちら
https://github.com/KakeruFujimiya/Prominence3D_Runtime_DX9/wiki