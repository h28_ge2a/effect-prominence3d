#pragma once
#include "MyGameEngine\Scene.h"

class Tank;
class PlayScene :public Scene
{
	Tank*	_tank;
	Fbx*	_ground;
	Fbx*	_wall;
	Quad	*_tree, *_tree2;

public:
	PlayScene();
	~PlayScene();
	void init()   override;
	void update() override;
	void input() override;
};

