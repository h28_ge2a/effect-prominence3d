#pragma once

#if _DEBUG
#pragma comment(lib, "P3DEngineD.lib")
#else
#pragma comment(lib, "P3DEngine.lib")
#endif


#include "MyGameEngine\Fbx.h"
#include "P3DEngine.h"



class Tank : public Fbx
{
	const float RUN_SPEED;
	const float ROTATE_SPEED;
	BOOL		_isWallHit;
	D3DXVECTOR3	_wallNormal;
	P3DEngine* pP3D;
	P3DEffect* pEffect = NULL;


public:
	Tank();
	~Tank();

	void init();
	void input() override;
	void draw() override;

	CREATE_FUNC(Tank);

	void collisionGround(Fbx* ground);
	void collisionWall(Fbx* wall);
};

