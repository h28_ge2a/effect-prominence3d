#include "Light.h"


Light::Light()
{
}


Light::~Light()
{
	
}

Light* Light::create()
{
	D3DLIGHT9 lightState;
	ZeroMemory(&lightState, sizeof(lightState));
	lightState.Type = D3DLIGHT_DIRECTIONAL;			//平行光源
	lightState.Direction = D3DXVECTOR3(1, -1, 1);	//光の方向

	//ライトの色
	lightState.Diffuse.r = 1.0f;
	lightState.Diffuse.g = 1.0f;
	lightState.Diffuse.b = 1.0f;

	//環境光
	lightState.Ambient.r = 1.0f;
	lightState.Ambient.g = 1.0f;
	lightState.Ambient.b = 1.0f;

	g.pDevice->SetLight(0, &lightState);	//ライト作成
	g.pDevice->LightEnable(0, TRUE);		//スイッチをON


	Light* light = new Light;
	return light;
}
