#pragma once
#include "Node.h"
#include <fbxsdk.h>

#pragma comment(lib,"libfbxsdk-mt.lib")




//レイキャスト用構造体
struct RayCastData
{
	D3DXVECTOR3 start;//レイ発射位置
	D3DXVECTOR3 dir;  //レイの向きベクトル
	float       dist; //衝突点までの距離
	BOOL        hit;  //レイが当たったか
	D3DXVECTOR3 normal;//法線
};



class Fbx :	public Node
{

	struct Vertex
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR3 normal;
		D3DXVECTOR2 uv;
	};

	FbxManager*  _manager;
	FbxImporter* _importer;
	FbxScene*    _scene;

	int _vertexCount;		//頂点数
	int _polygonCount;		//ポリゴ数
	int _indexCount;		//インデックス数
	int _materialCount;		//マテリアルの個数
	int* _polygonCountOfMaterial;	//マテリアルごとのポリゴン数

	LPDIRECT3DVERTEXBUFFER9		_vertexBuffer;	//頂点バッファ
	LPDIRECT3DINDEXBUFFER9*		_indexBuffer;	//インデックスバッファ
	D3DMATERIAL9*				_material;		//マテリアル
	LPDIRECT3DTEXTURE9*			_pTexture;		//テクスチャ


	void checkNode(FbxNode* pNode);
	void checkMesh(FbxMesh* pMesh);
	D3DXMATRIX createWorldMatrix();

public:
	Fbx();
	~Fbx();
	static Fbx* create(LPCSTR fileName);
	void load(LPCSTR fileName);
	void draw();
	void rayCast(RayCastData *data);
};

